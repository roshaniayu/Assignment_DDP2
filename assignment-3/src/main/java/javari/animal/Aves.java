package javari.animal;

public class Aves extends Animal {

    private boolean isLayingEgg = false;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                   double weight, String layingEgg, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (layingEgg.equals("laying egg")) {
            this.isLayingEgg = true;
        }
    }

    public boolean specificCondition() {
        if (this.isLayingEgg) {
            return false;
        } else {
            return true;
        }
    }
}
