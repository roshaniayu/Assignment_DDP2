package javari.animal;

public class Mammals extends Animal {

    private boolean isPregnant = false;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (pregnant.equals("pregnant")) {
            this.isPregnant = true;
        }
    }

    public boolean specificCondition() {
        if (this.getType().equals(("Lion")) && this.getGender().equals(Gender.FEMALE)) {
            return false;
        } else if (this.isPregnant) {
            return false;
        } else {
            return true;
        }
    }
}
