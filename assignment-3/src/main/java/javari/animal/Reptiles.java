package javari.animal;

public class Reptiles extends Animal{

    private boolean isTame;

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                    double weight, String tame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (tame.equals("tame")) {
            this.isTame = true;
        } else if (tame.equals("wild")) {
            this.isTame = false;
        }
    }

    public boolean specificCondition() {
        if (!this.isTame) {
            return false;
        } else {
            return true;
        }
    }
}
