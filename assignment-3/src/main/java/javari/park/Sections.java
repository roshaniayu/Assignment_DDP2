package javari.park;

import java.util.*;

public class Sections {

    private String name;
    private List<String> animal = new ArrayList<String>();

    public Sections(String name) {
        this.name = name;
    }

    public void addAnimal(String type) {
        animal.add(type);
    }

    public List<String> getAnimal() {
        return animal;
    }

    public String getName() {
        return name;
    }
}
