//Contributor = Khalis Murfid

package javari;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.RegistrationWriter;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.nio.file.Paths;

public class A3Festival {
    public static void main(String args[]){

        ArrayList<Animal> listAnimals = new ArrayList<Animal>();
        ArrayList<Attractions> listaAttractions = new ArrayList<Attractions>();
        ArrayList<Sections> listSections = new ArrayList<Sections>();
        ArrayList<Visitors> registeredVisitor = new ArrayList<Visitors>();
        int id = 1;
        String directory = "";
        Scanner input = new Scanner(System.in);
        CsvReader categories, records, attractions;
        ReaderSections sections;

        while (true) {
            System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
            try {
                File CsvSections = new File(directory + "/animals_categories.csv");
                File CsvAttractions = new File(directory + "/animals_attractions.csv");
                File CsvAnimalCategories = new File(directory + "/animals_categories.csv");
                File CsvAnimalRecords = new File(directory + "/animals_records.csv");

                sections = new ReaderSections(CsvSections.toPath());
                attractions = new ReaderAttractions(CsvAttractions.toPath());
                categories = new ReaderAnimalCategories(CsvAnimalCategories.toPath());
                records = new ReaderAnimalRecords(CsvAnimalRecords.toPath());

                System.out.println("... Loading... Success... System is populating data...\n");
                System.out.println("Found " + sections.countValidRecords() + " valid sections and " + sections.countInvalidRecords() + " invalid sections");
                System.out.println("Found " + attractions.countValidRecords() + " valid attractions and " + attractions.countInvalidRecords() + " invalid attractions");
                System.out.println("Found " + categories.countValidRecords() + " valid animal categories and " + categories.countInvalidRecords() + " invalid animal categories");
                System.out.println("Found " + records.countValidRecords() + " valid animal records and " + records.countInvalidRecords() + " invalid animal records\n");

                //Make Sections
                for (String section : sections.getSections()) {
                    listSections.add(new Sections(section));
                }

                //Make Attractions
                for (String attraction : attractions.getLines()) {
                    String[] data = attraction.split(",");
                    listaAttractions.add(new Attractions(data[0], data[1]));
                }

                //Make Animal Categories
                for (String category : categories.getLines()) {
                    String[] data = category.split(",");
                    for (Sections section : listSections) {
                        if (data[2].equals(section.getName())) {
                            section.addAnimal(data[0]);
                        }
                    }
                }

                //Make Animal Records
                for (String record : records.getLines()) {
                    String[] data = record.split(",");
                    if (data[1].equals("Cat") || data[1].equals("Lion") || data[1].equals("Hamster") || data[1].equals("Whale")) {
                        listAnimals.add(new Mammals(Integer.parseInt(data[0]), data[1], data[2], Gender.parseGender(data[3]), Double.parseDouble(data[4]),
                                        Double.parseDouble(data[5]), data[6], Condition.parseCondition(data[7])));
                    }
                    if(data[1].equals("Parrot") || data[1].equals("Eagle")){
                        listAnimals.add(new Aves(Integer.parseInt(data[0]), data[1], data[2], Gender.parseGender(data[3]), Double.parseDouble(data[4]),
                                        Double.parseDouble(data[5]), data[6], Condition.parseCondition(data[7])));
                    }
                    if(data[1].equals("Snake")){
                        listAnimals.add(new Reptiles(Integer.parseInt(data[0]), data[1], data[2], Gender.parseGender(data[3]), Double.parseDouble(data[4]),
                                        Double.parseDouble(data[5]), data[6], Condition.parseCondition(data[7])));
                    }
                }

                //Add performer (animals) to attraction
                for (Attractions attraction : listaAttractions){
                    for (Animal animal : listAnimals){
                        attraction.addPerformer(animal);
                    }
                }
                break;
            }
            catch (IOException e){
                System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: \n");
                directory = input.nextLine();
            }
        }

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");

        mainmenu:
        while (true) {
            //Access Park
            int index = 1;
            System.out.println("Javari Park has "+ sections.countValidRecords() +" sections:");
            for (Sections section : listSections) {
                System.out.println(index + ". " + section.getName());
                index++;
            }

            //Access Section based on Preferred Input
            System.out.print("Please choose your preferred section (type the number): ");
            int chosenSection = Integer.parseInt(input.nextLine());
            
            animalmenu:
            while (true) {
                index = 1;
                System.out.println("\n--" + listSections.get(chosenSection-1).getName() + "--");
                for (String type : listSections.get(chosenSection-1).getAnimal()) {
                    System.out.println(index + ". " + type);
                    index++;
                }


                //Access Animal based on Preferred Input
                System.out.print("Please choose your preferred animals (type the number): ");
                String chosenAnimal = input.nextLine();
                if (chosenAnimal.equals("#")) { //Return to the Previous Menu
                    System.out.println();
                    break animalmenu;
                }

                String animalType = listSections.get(chosenSection-1).getAnimal().get(Integer.parseInt(chosenAnimal)-1);
                // Check Availability of Performers
                boolean avalaibility = false;
                for (Attractions attraction : listaAttractions) {
                    if (!attraction.getPerformers().isEmpty()) {
                        for (Animal animal : attraction.getPerformers()) {
                            if (animal.getType().equals(animalType)) {
                                avalaibility = true;
                            }
                        }
                    }
                }
                if (!avalaibility) {
                    System.out.println("\nUnfortunately, no " + animalType + " can perform any attraction, please choose other animals\n");
                    break animalmenu;
                }

                System.out.println("\n---" + animalType + "---\n" + "Attractions by " + animalType + ":");
                index = 1;
                ArrayList<Attractions> option = new ArrayList<>();
                for (Attractions attraction : listaAttractions){
                    if (!attraction.getPerformers().isEmpty()) {
                        if (attraction.getType().equals(animalType)){
                            option.add(attraction);
                            System.out.println(index + ". " + attraction.getName());
                            index++;
                        }
                    }
                }

                System.out.print("Please choose your preferred attractions (type the number): ");
                String chooseAttraction = input.nextLine();
                
                attractionmenu:
                while (true) {
                    if (chooseAttraction.equals("#")) { //Return to the Previous Menu
                        break attractionmenu;
                    }

                    Attractions chosenAttraction = option.get(Integer.parseInt(chooseAttraction)-1);
                    System.out.print("\nWow, one more step,\n" + "please let us know your name: ");
                    String visitorName = input.nextLine();

                    System.out.print("\nYeay, final check!\n" +"Here is your data, and the attraction you chose:\n" +"Name: " + visitorName + "\n" +
                                    "Attractions: " + chosenAttraction.getName() + " -> " + animalType + "\n" +
                                     "With: ");

                    int i = 0;
                    for (Animal performers : chosenAttraction.getPerformers()) {
                        if (performers.getType().equals(animalType)) {
                            System.out.print(chosenAttraction.getPerformers().get(i).getName());
                            if (i != chosenAttraction.getPerformers().size()-1) {
                                System.out.print(", ");
                            }
                        }
                        i++;
                    }

                    System.out.println("\n");
                    System.out.print("Is the data correct? (Y/N): ");
                    String confirmation = input.nextLine();
                    if (confirmation.equals("N")) {
                        System.out.println("Unregistering ticket. Back to main menu.\n");
                        break animalmenu;
                    }
                    boolean alreadyRegistered = false;
                    for (Visitors visitor : registeredVisitor) {
                        if (visitor.getVisitorName().equals(visitorName)) {
                            visitor.addSelectedAttraction(chosenAttraction);
                            alreadyRegistered = true;
                        }
                    }
                    if (!alreadyRegistered) {
                        Visitors visitor = new Visitors(id, visitorName);
                        registeredVisitor.add(visitor);
                        visitor.addSelectedAttraction(chosenAttraction);
                        id++;
                    }

                    System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
                    String anotherTicket = input.nextLine();

                    if (anotherTicket.equals("Y")) {
                        break animalmenu;
                    }
                    System.out.print("");

                    //Writing Json file as an output from the program
                    for (Registration visitor : registeredVisitor) {
                        try {
                            RegistrationWriter.writeJson(visitor, Paths.get("/Users/roshaniayupranasti/Desktop/"));
                        }
                        catch(IOException e){
                            continue;
                        }
                    }
                    break mainmenu;
                }
            }
        }
    }
}
