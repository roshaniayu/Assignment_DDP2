package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;

public class ReaderAnimalCategories extends CsvReader {

    private List<String> categories = new ArrayList<String>();

    public ReaderAnimalCategories(Path File) throws IOException {
        super(File);
    }

    public List<String> getCategories() {
        return categories;
    }

    public long countValidRecords() {
        for(String line : lines){
            String[] data = line.split(COMMA);
            try {
                for (String category : categories) {
                    if (data[1].equals(category)) {
                        throw new Exception();
                    }
                }
                categories.add(data[1]);
            }
            catch(Exception e){
                continue;
            }
        }
        return (long) categories.size();
    }

    public long countInvalidRecords() {
        long invalid = 0;
        for (String line : lines) {
            String[] data = line.split(COMMA);
            if (data.length < 2) {
                invalid += 1;
            }
        }
        return invalid;
    }
}
