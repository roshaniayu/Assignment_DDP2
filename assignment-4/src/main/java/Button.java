import javax.swing.*;

public class Button extends JButton {

 	private ImageIcon image;
 	private Button self;

	public Button(ImageIcon image, int x, int y, JLabel label) {
		super(image);
		this.image = image;
		setBounds(x,y,125,125);
	}

	public void setButton(Button self, JLabel label) {
		this.self = self;
    	addActionListener(new ButtonAction(self, label));
	}

	public ImageIcon getImage() {
    	return image;
	}
}