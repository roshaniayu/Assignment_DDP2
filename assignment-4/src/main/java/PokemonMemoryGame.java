import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class PokemonMemoryGame {
	public static ArrayList<ImageIcon> imageList = new ArrayList<ImageIcon>();
	JFrame frame = new JFrame("Pokemon Memory Game");

	public PokemonMemoryGame() {
		loadImage();
		randomizeImage();
		generateGUI();
	}

	public static void loadImage() {
		File folder = new File("pokemon");
		File[] listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
			if ((file.getName().length() == 14)) {
				imageList.add(new ImageIcon(file.getAbsolutePath()));
			}
		}
		for (int i = 0; i < 18; i++) {
			imageList.add(imageList.get(i));
		}
	}

	public static void randomizeImage() {
		Random random = ThreadLocalRandom.current();
    	for (int i = imageList.size() - 1; i > 0; i--) {
      		int index = random.nextInt(i + 1);
      		ImageIcon temp = imageList.get(index);
      		imageList.set(index, imageList.get(i));
      		imageList.set(i, temp);
    	}
	}

	public static void clearImageList() {
		imageList.clear();
	}

  	public  void restartButton(){
        Object[] choice = {"Play Again", "Cancel"};
        int option = JOptionPane.showOptionDialog(null, "                    Are you sure?", "Restart Game",
                	 JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, choice, choice[1]);
        if (option == 0) {
            clearImageList();
            frame.dispose();
            new PokemonMemoryGame();
        }
    }

  	public void generateGUI() {
  		JButton restart = new JButton("Play Again");
	    JLabel label = new JLabel("Number of Attempt(s): 0");
		int x = 3, y = 3, index = 0;
		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= 6; j++) {
				Button button = new Button(imageList.get(index),x,y,label);
				button.setButton(button,label);
				frame.add(button);
				y += 125;
				index++;
			}
			y = 3;
			x += 125;
		}
	    restart.setBounds(0, 753, 100, 30);
    	restart.addActionListener(actions -> restartButton());
	    label.setBounds(100, 753, 200, 30);
	    frame.add(restart);
	    frame.add(label);
	    frame.setSize(756,810);
	    frame.setLayout(null);
	    frame.setVisible(true);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 	}

	public static void main(String[] args) {
		PokemonMemoryGame game = new PokemonMemoryGame();
	}
}