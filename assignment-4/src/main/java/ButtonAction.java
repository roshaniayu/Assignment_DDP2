import javax.swing.*;
import java.awt.event.*;

public class ButtonAction implements ActionListener {

    public static ImageIcon cover = new ImageIcon("pokemon/pokeball.png");
    public static boolean inDelay = false;
    public static int attempt = 0;
    public static Button prev = null;
    private Button self;
    private JLabel label;

    public ButtonAction(Button self, JLabel label) {
        this.self = self;
        this.label = label;
        self.setIcon(cover);
    }

  	private ActionListener match = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
            self.setVisible(false);
      		  prev.setVisible(false);
      		  prev = null;
            inDelay = false;
        }
  	};

  	private ActionListener notMatch = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
      		  self.setIcon(cover);
      		  prev.setIcon(cover);
      		  prev = null;
            inDelay = false;
        }
  	};

  	@Override
  	public void actionPerformed(ActionEvent e) {
        if (inDelay) {
            return;
        }
        if (prev == null) {
          	prev = self;
          	self.setIcon(self.getImage());
        } else if (prev.getImage() == self.getImage() && self != prev) {
            label.setText("Number of Attempt(s): " + ++attempt);
            self.setIcon(self.getImage());
            inDelay = true;
            Timer timer = new Timer(500, match);
            timer.setRepeats(false);
            timer.start(); 
        } else {
            label.setText("Number of Attempt(s): " + ++attempt);
          	self.setIcon(self.getImage());
            inDelay = true;
          	Timer timer = new Timer(500, notMatch);
          	timer.setRepeats(false);
          	timer.start();
        }
    }
}