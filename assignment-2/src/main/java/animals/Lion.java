package animals;

public class Lion extends Animals {

    //Constructor of Lion
    public Lion(String name, int length) {
        super(name, length, true);
    }

    //Methods
    public void action(int number) {
        if (number == 1) {
            this.seeItHunting();
        } else if (number == 2) {
            this.brushTheMane();
        } else if (number == 3) {
            this.disturbIt();
        } else {
            System.out.println(this.getName() + " says: HM?");
        }
        
        System.out.println("Back to the office!\n");
    }

    private void seeItHunting() {
    	System.out.println("Lion is hunting..");
        System.out.printf("%s makes a voice: err...!%n", this.getName());
    }

    private void brushTheMane() {
    	System.out.println("Clean the lion’s mane..");
		System.out.printf("%s makes a voice: Hauhhmm!%n", this.getName());
    }

    private void disturbIt() {
        System.out.printf("%s makes a voice: HAUHHMM!%n", this.getName());
    }
}