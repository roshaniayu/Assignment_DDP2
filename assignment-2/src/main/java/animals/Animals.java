package animals;

public class Animals {

    //Instance Variable of Animals
    protected String name, location;
    protected int length, lengthOfCage, widthOfCage;
    protected boolean isWild;
    protected char cageType;

    //Constructor of Animals
    public Animals(String name, int length, boolean isWild) {
        this.name = name;
        this.length = length;
        this.isWild = isWild;
        if (isWild == false) {
        	this.location = "indoor";
			if (length < 45) {
				this.lengthOfCage = 60;
				this.widthOfCage = 60;
				this.cageType = 'A';
			} else if (length <= 60) {
				this.lengthOfCage = 60;
				this.widthOfCage = 90;
				this.cageType = 'B';
			} else {
				this.lengthOfCage = 60;
				this.widthOfCage = 120;
				this.cageType = 'C';
			}
		} else {
			this.location = "outdoor";
			if (length < 75) {
				this.lengthOfCage = 120;
				this.widthOfCage = 120;
				this.cageType = 'A';
			} else if (length <= 90) {
				this.lengthOfCage = 120;
				this.widthOfCage = 150;
				this.cageType = 'B';
			} else {
				this.lengthOfCage = 120;
				this.widthOfCage = 180;
				this.cageType = 'C';
			}
		}
    }

    //Getter & Setter
    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public String getLocation() {
        return location;
    }

    public boolean getIsWild() {
        return isWild;
    }

    public char getCageType() {
        return cageType;
    }

    //Overriding Method with Subclasses
    public void action(int number) {
    }
}