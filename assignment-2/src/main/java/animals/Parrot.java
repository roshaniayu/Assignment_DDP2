package animals;
import java.util.Scanner;

public class Parrot extends Animals {

	//Constructor of Parrot
    public Parrot(String name, int length) {
        super(name, length, false);
    }

    //Methods
    public void action(int number) {
        if (number == 1) {
            this.ordertoFly();
        } else if (number == 2) {
            this.doConversation();
        } else {
            System.out.printf("%s says: HM?", this.getName());
        }

        System.out.println("Back to the office!\n");
    }

    private void ordertoFly() {
        System.out.printf("Parrot %s flies!%n", this.getName());
		System.out.printf("%s makes a voice: FLYYYY.....%n", this.getName());
    }

    private void doConversation() {
        Scanner input = new Scanner(System.in);
        System.out.print("You say: ");
        String command = input.nextLine();
        System.out.printf("%s says: %s%n", this.getName(), command.toUpperCase());
    }
}