package animals;
import java.util.Arrays;
import java.util.Random;

public class Cat extends Animals {

    //Constructor of Cat
    public Cat(String name, int length) {
        super(name, length, false);
    }

    //Methods
    public void action(int number) {
        if (number == 1) {
            this.brushTheFur();
        } else if (number == 2) {
            this.cuddle();
        } else {
            System.out.println("You do nothing...");
        }

        System.out.println("Back to the office!\n");
    }

    private void brushTheFur() {
        System.out.printf("Time to clean %s's fur%n", this.getName());
        System.out.printf("%s makes a voice: Nyaaan...%n", this.getName());
    }

    private void cuddle() {
    	String[] catVoices = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    	Random randomize = new Random();
        System.out.printf("%s makes a voice: %s%n", this.getName(), catVoices[randomize.nextInt(4)]);
    }
}