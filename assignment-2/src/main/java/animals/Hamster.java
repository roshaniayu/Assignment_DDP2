package animals;

public class Hamster extends Animals {

	//Constructor of Hamster
    public Hamster(String name, int length) {
        super(name, length, false);
    }

    //Methods
    public void action(int number) {
        if (number == 1) {
            this.seeItGnawing();
        } else if (number == 2) {
            this.orderToRun();
        } else {
            System.out.println("You do nothing...");
        }

        System.out.println("Back to the office!\n");
    }

    private void seeItGnawing() {
       System.out.printf("%s makes a voice: ngkkrit.. ngkkrrriiit%n", this.getName());
    }

    private void orderToRun() {
        System.out.printf("%s makes a voice: trrr.... trrr...%n", this.getName());
    }
}
