package animals;

public class Eagle extends Animals {

	//Constructor of Eagle
  	public Eagle(String name, int length) {
        super(name, length, true);
    }

    //Methods
    public void action(int number) {
        if (number == 1) {
            this.orderToFly();
        } else {
            System.out.println("You do nothing...");
        }

        System.out.println("Back to the office!\n");
    }

    private void orderToFly() {
        System.out.printf("%s makes a voice: kwaakk...&n", this.getName());
        System.out.println("You hurt!");
    }
}