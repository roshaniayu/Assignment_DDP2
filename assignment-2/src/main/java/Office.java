import java.util.Scanner;
import java.util.ArrayList;
import cages.Cages;
import animals.*;

public class Office {

    //Instance Variable of Office
    private static ArrayList<Animals> listOfCat = new ArrayList<>();
    private static ArrayList<Animals> listOfLion = new ArrayList<>();
    private static ArrayList<Animals> listOfEagle = new ArrayList<>();
    private static ArrayList<Animals> listOfParrot = new ArrayList<>();
    private static ArrayList<Animals> listOfHamster = new ArrayList<>();


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Office javariPark = new Office();
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        int[] number = new int[5];
        String[] typeOfAnimals = {"cat", "lion", "eagle", "parrot", "hamster"};

        for (int i = 0; i < 5; i++) {
            System.out.print(typeOfAnimals[i] + ": ");
            int amount = input.nextInt();
            number[i] = amount;
            if (amount > 0) {
                javariPark.initiation(typeOfAnimals[i]);
            }
        }
        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");

        javariPark.printArrangement();

        System.out.println("NUMBER OF ANIMALS:");
        for (int k = 0; k < 5; k++){
            System.out.println(typeOfAnimals[k] + ":" + number[k]);
        }
        System.out.println("\n=============================================");

        javariPark.visitAnimal();
    }

    //Methods
    private void initiation(String type) {
        Scanner input = new Scanner(System.in);
        System.out.println("Provide the information of " + type + "(s):");
        String[] informations = input.nextLine().split(",");

        for (String temp : informations) {
            String[] information = temp.split("\\|");
            if (type.equals("cat")) {
                Cat cat = new Cat(information[0], Integer.parseInt(information[1]));
                listOfCat.add(cat);
            } else if (type.equals("hamster")) {
                Hamster hamster = new Hamster(information[0], Integer.parseInt(information[1]));
                listOfHamster.add(hamster);
            } else if (type.equals("parrot")) {
                Parrot parrot = new Parrot(information[0], Integer.parseInt(information[1]));
                listOfParrot.add(parrot);
            } else if (type.equals("lion")) {
                Lion lion = new Lion(information[0], Integer.parseInt(information[1]));
                listOfLion.add(lion);
            } else if (type.equals("eagle")) {
                Eagle eagle = new Eagle(information[0], Integer.parseInt(information[1]));
                listOfEagle.add(eagle);
            }
        }
    }

    private void printArrangement() {
        System.out.println("Cage arrangement:");
        if (listOfCat.size() != 0) {
            Cages catCage = new Cages(listOfCat);
            catCage.arrangeCages();
            catCage.rearrangeCages();
        }
        if (listOfLion.size() != 0) {
            Cages lionCage = new Cages(listOfLion);
            lionCage.arrangeCages();
            lionCage.rearrangeCages();
        }
        if (listOfEagle.size() != 0) {
            Cages eagleCage = new Cages(listOfEagle);
            eagleCage.arrangeCages();
            eagleCage.rearrangeCages();
        }
        if (listOfParrot.size() != 0) {
            Cages parrotCage = new Cages(listOfParrot);
            parrotCage.arrangeCages();
            parrotCage.rearrangeCages();
        }
        if (listOfHamster.size() != 0) {
            Cages hamsterCage = new Cages(listOfHamster);
            hamsterCage.arrangeCages();
            hamsterCage.rearrangeCages();
        }
    }

    private void visitAnimal() {
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.println("Which animal do you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: exit)");
            String choice = input.nextLine();
            boolean availability = false;

            if (choice.equals("1")) {
                if (listOfCat.size() == 0) {
                    System.out.println("There is no cat to visit");
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.print("Mention the name of the cat you want to visit: ");
                    String tempName = input.nextLine();

                    for (Animals tempAnimal : listOfCat) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.printf("You are visiting %s (cat) now, what would you like to do?%n", tempName);
                            System.out.println("1: Brush the fur 2: Cuddle");
                            int tempNumber = input.nextInt();
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }

                    if (!availability) {
                        System.out.println("There is no cat with that name! Back to the office!\n");
                    }
                }
            } else if (choice.equals("2")) {
                if (listOfEagle.size() == 0) {
                    System.out.println("There is no eagle to visit");
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.print("Mention the name of the eagle you want to visit: ");
                    String tempName = input.nextLine();

                    for (Animals tempAnimal : listOfEagle) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.printf("You are visiting %s (eagle) now, what would you like to do?%n", tempName);
                            System.out.println("1: Order to fly");
                            int tempNumber = input.nextInt();
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }

                    if (!availability) {
                        System.out.println("There is no eagle with that name! Back to the office!\n");
                    }
                }
            } else if (choice.equals("3")) {
                if (listOfHamster.size() == 0) {
                    System.out.println("There is no hamster to visit");
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.print("Mention the name of the hamster you want to visit: ");
                    String tempName = input.nextLine();

                    for (Animals tempAnimal : listOfHamster) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.printf("You are visiting %s (hamster) now, what would you like to do?%n", tempName);
                            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                            int tempNumber = input.nextInt();
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }

                    if (!availability) {
                        System.out.println("There is no hamster with that name! Back to the office!\n");
                    }
                }
            } else if (choice.equals("4")) {
                if (listOfParrot.size() == 0) {
                    System.out.println("There is no parrot to visit");
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.print("Mention the name of the parrot you want to visit: ");
                    String tempName = input.nextLine();

                    for (Animals tempAnimal : listOfParrot) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.printf("You are visiting %s (parrot) now, what would you like to do?%n", tempName);
                            System.out.println("1: Order to fly 2: Do conversation");
                            int tempNumber = input.nextInt();
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }

                    if (!availability) {
                        System.out.println("There is no parrot with that name! Back to the office!\n");
                    }
                }
            } else if (choice.equals("5")) {
                if (listOfLion.size() == 0) {
                    System.out.println("There is no lion to visit");
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.print("Mention the name of the lion you want to visit: ");
                    String tempName = input.nextLine();

                    for (Animals tempAnimal : listOfLion) {
                        if (tempName.equalsIgnoreCase(tempAnimal.getName())) {
                            System.out.printf("You are visiting %s (lion) now, what would you like to do?%n", tempName);
                            System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                            int tempNumber = input.nextInt();
                            tempAnimal.action(tempNumber);
                            availability = true;
                        }
                    }

                    if (!availability) {
                        System.out.println("There is no lion with that name! Back to the office!\n");
                    }
                }
            } else if (choice.equals("99")){
                break;
            } else {
                System.out.println("Input number between 1-5 to visit one of the animals or 99 to exit the program\n");;
            }
        }
    }
}