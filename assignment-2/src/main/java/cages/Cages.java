package cages;
import java.util.ArrayList;
import animals.Animals;

public class Cages {

    //Instance Variable of Cages
    private ArrayList<Animals> list;
    private Animals listOfLevels[][];
    private int size;

    //Constructor of Cages
    public Cages(ArrayList<Animals> list) {
        this.list = list;
    }

    //Methods
    public void arrangeCages() {
        int index = 0;

        //If the number of cages made is overloaded
        if (list.size() > ((list.size() / 3) * 3)) {
            size = list.size() / 3; //how many rows
            index = 3 - (list.size() - size * 3); // where the overload start
        } else {
            size = list.size() / 3 - 1;
        }

        //Creating a new list for the arranged cage
        listOfLevels = new Animals[3][size + 1];
        int index2 = 0;

        //Arranging from top to bottom (list.size <= 3)
        if (size == 0) {
            for (int a = 0; a < list.size(); a++) {
                listOfLevels[a][0] = list.get(a);
            }
        //(list.size > 3) 
        } else {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < size + 1; j++) {
                    if (j == size && i < index) {
                        break;
                    } else {
                        listOfLevels[i][j] = list.get(index2);
                        index2++;
                    }
                }
            }
        }

        System.out.println("location: " + listOfLevels[0][0].getLocation());
        printAnimal(listOfLevels, size);
        System.out.println();
    }

    public void rearrangeCages() {
        Animals listAfterArrangement[][];
        listAfterArrangement = new Animals[3][size + 1];
        int temp1 = 1;
        //Reversing the arrangement of cages (index 0-1, 1-2, 2-0)
        for (int i = 2; i >= 0; i--) {
            int temp2 = size;
            if (temp1 == -1) {
                temp1 = 2;
            }

            for (int j = 0; j < size + 1; j++) {
                listAfterArrangement[i][j] = listOfLevels[temp1][temp2];
                temp2--;
            }

            temp1--;
        }

        System.out.println("After rearrangement...");
        printAnimal(listAfterArrangement, size);
        System.out.println();
    }

    private void printAnimal(Animals listOfAnimal[][], int size) {
        //printing from bottom to top
        for (int i = 2; i >= 0; i--) {
            System.out.print("Level " + (i + 1) + ": ");
            for (int j = 0; j < size + 1; j++) {
                if (listOfAnimal[i][j] != null) {
                    System.out.print(listOfAnimal[i][j].getName() + "(" + 
                                     listOfAnimal[i][j].getLength() + " - " +
                                     listOfAnimal[i][j].getCageType() + "), ");
                }
            }
            
            System.out.println();
        }
    }
}