public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    public WildCat cat;
    public TrainCar next;

    // constructor overloading
    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    // methods
    public double computeTotalWeight() {
        // TODO Complete me!
        if (next == null) {
            return cat.weight + EMPTY_WEIGHT;
        } else {
            return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if (next == null) {
            System.out.println("(" + cat.name + ")");
        } else {
            System.out.print("(" + cat.name + ")" + "--");
            next.printCar();
        }
    }
}
