public class WildCat {

    // TODO Complete me!
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    // constructor
    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // method
    public double computeMassIndex() {
        // TODO Complete me!
    	double bmi = weight / Math.pow(length * 0.01, 2);
        return bmi;
    }
}
